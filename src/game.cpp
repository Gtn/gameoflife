/*
 * <Game of Life, simple game of life board>
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "game.h"

Game::Game(int argc, char *argv[]) : m_font_sans(sf::Font()), m_text_pause(sf::Text("Pause Game", m_font_sans, 40)), m_menu(m_window)
{
    m_update_time = 0;
    m_error_state = EXIT_SUCCESS;

    int x(90), y(70), size(10);
    if(argc == 3 || argc == 4) {
        x = std::stoi(argv[1]);
        y = std::stoi(argv[2]);
    }

//     x = (x > 110) ? 110 : x;
//     y = (y > 70) ? 70 : y;

    std::cout << x << y << std::endl;
    
    if(argc == 4) {
        size = std::stoi(argv[3]);
    }
    
//     size = (size * x > 1100) ? 1100 / x : size;
//     size = (size * y > 750) ? 750 / y : size;
    
    m_size = size;
    m_game_board.init_board(x, y, 50, size);
    
	// if(x > 100 || y > 60)
 //         m_window.create(sf::VideoMode(1366, 768), "GoL");
 //    else
	    m_window.create(sf::VideoMode(size * x + 250, size * y), "GoL");
    
    m_menu.setPosition(m_game_board.get_x_size() * size + 20, 40);
    
    gui::Theme::loadFont("resources/sansation.ttf");
    gui::Theme::loadTexture("resources/default_texture.png");
    gui::Theme::textSize = 11;
    gui::Theme::click.textColor      = sf::Color::White;
    gui::Theme::click.textColorFocus = sf::Color::Black;
    gui::Theme::input.textColor =      sf::Color::Black;
    gui::Theme::click.textColorHover = sf::Color::White;
    gui::Theme::input.textColorHover = sf::Color::Black;
    gui::Theme::input.textColorFocus = sf::Color::Black;
    gui::Theme::PADDING = 2.f;
    gui::Theme::windowBgColor = sf::Color::Black;
    
    m_slider = new gui::Slider();
    m_slider->setStep(1);
    m_slider->setCallback([&]() {
        m_update_time = m_slider->getValue();
    });
    
    m_button = new gui::Button("clear");
    m_button->setCallback([&]() {
       m_game_board.clear(); 
    });
    
    m_menu.add(m_slider);
    m_menu.add(m_button);
    
    m_left_panel_hidder.setSize({300, 500});
    m_left_panel_hidder.setPosition(m_game_board.get_x_size() * size, 0);
    m_left_panel_hidder.setFillColor(sf::Color::Black);
    
    
    m_font_sans.loadFromFile("resources/sansation.ttf");
    
    m_text_pause.setPosition(m_game_board.get_x_size() * size + 20, 0);
    m_text_pause.setCharacterSize(30);
        
    m_pause = false;
        
    
//     std::cout << EXIT_SUCCESS << EXIT_FAILURE << " " << m_error_state << std::endl;
    m_last_update = std::chrono::steady_clock::now();
}

void Game::event_mgr() {
    if(m_window.isOpen()) {
        
        sf::Event event;
        
        while (m_window.pollEvent(event)) {
            if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space) {
                m_pause = !m_pause;
                
                if(m_pause) {
                    draw_board();
                    m_window.draw(m_text_pause);
                } else {
                    m_window.clear(sf::Color::Black);
                }
            }
            
            // Mouse button event
            if(event.type == sf::Event::MouseButtonPressed) {
                sf::Vector2i mousePos = sf::Mouse::getPosition(m_window);
//                 std::cout << "DEBUG : Mouse : clicked : left at : " << mousePos.x << ";" << mousePos.y << std::endl;
                
                // If in board
                if(m_game_board.is_in_board(int(mousePos.x / m_size), int(mousePos.y / m_size))) {
                    // reverse state of case cliked
                    if(m_game_board.get_state(int(mousePos.x / m_size), int(mousePos.y / m_size)) == 0) {
                        m_game_board.set_state(int(mousePos.x / m_size), int(mousePos.y / m_size), 1);
                    } else {
                        m_game_board.set_state(int(mousePos.x / m_size), int(mousePos.y / m_size), 0);;
                    }
                    
                    m_window.draw(* m_game_board.get_sprite(int(mousePos.x / m_size), int(mousePos.y / m_size))); // draw modified sprite
                    m_window.display(); // Redisplay instantly, don't wait next automatic display
                }
            }
   
            m_menu.onEvent(event);
   
            // Close m_window: exit
            if (event.type == sf::Event::Closed)
                m_window.close();
            
//             m_slider.event_mgr(event);
        }
    }
}

void Game::draw_board() {
    for(unsigned long int i(0); i < m_game_board.get_x_size(); i++) {
        for(unsigned long int j(0); j < m_game_board.get_y_size(); j++) {
            m_window.draw(* m_game_board.get_sprite(i,j));
        }
    }
}

void Game::clear_panel() {
    m_window.draw(m_left_panel_hidder);
}

int Game::launch_main_loop() {
    std::cout << "launching main loop" << std::endl;
    while (m_window.isOpen()) {
        
        if(m_error_state != EXIT_SUCCESS) {
            m_window.close();
            return m_error_state;
        }
        
        event_mgr();

//         clear_panel();
        m_window.draw(m_left_panel_hidder);
//         m_window.clear(gui::Theme::windowBgColor);
        m_window.draw(m_menu);
        
        if(!m_pause) {

            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
//             std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - m_last_update).count() << std::endl;
//             std::cout << m_update_time << std::endl;
            if(std::chrono::duration_cast<std::chrono::milliseconds>(end - m_last_update).count() > (100 - m_update_time) * 10) {
//                 std::cout << "UPDATE" << std::endl;
                m_last_update = end;
                m_game_board.update();
            }
        } else {
            m_window.draw(m_text_pause);
        }
        draw_board();


        m_window.display();
    }
    return m_error_state;
}

Game::~Game() {
    delete m_button;
    delete m_slider;
}

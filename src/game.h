/*
 * Game of Life, simple game of life board
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAME_H
#define GAME_H



#include "board.h"
#include <iostream>
#include <chrono>
// #include <thread>
// #include <time.h>

//#include "slider.h"

#include "Gui/Menu.hpp"
#include "Gui/Theme.hpp"
#include "Gui/Gui.hpp"

/**
 * @todo write docs
 */

class Game
{
public:
    /**
     * Default constructor
     */
    Game(int argc, char *argv[]);
    ~Game();
    void draw_board();
    void event_mgr();
    int launch_main_loop();
    void clear_panel();
    
private:
    
    sf::Font m_font_sans;
    Board m_game_board;
    bool m_changed, m_pause;
    sf::RenderWindow m_window;
    sf::Text m_text_pause;
    int m_error_state, m_update_time, m_size;

    gui::Menu m_menu;
    gui::Slider* m_slider;
    gui::Button* m_button;
    sf::RectangleShape m_left_panel_hidder;
    std::chrono::steady_clock::time_point m_last_update;

};

#endif // GAME_H

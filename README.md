# GameOfLife

Simple Conway's game of life

Made with love in c++ with the help of sfml and sfml-widget, from https://github.com/abodelot/sfml-widgets

Licensed under GplV3, of which you have a copy in the directory

To launch, just clone/download project (or just the build folder)
Then open a terminal, go to build (`cd ~/Downloads/gameoflife/build`), and then type `./gameoflife`
If it says that you're missing a sfml library, just install sfml, with `sudo apt install libsfml-audio2.5 libsfml-graphics2.5`

To build it, you also need to install libsfml-dev

You can change the board size : 
`./gameoflife 40 60` will create a 40\*60 board
`./gameoflife 40 60 20` will create a 40\*60 board with 20\*20 pixels cases

You can change the state of a case by clicking on it

Known bugs : clicking on a case doesn't work when changing the window size

CC=g++

CCOPTION = Wall std=gnu++11
FLAGS = $(addprefix -,$(CCOPTION))

OBJ = main.o board.o game.o case_object.o

OBJ_DIR = obj
LIB_DIR = lib
BUILD_DIR = build
SRC_DIR = src
LDLIBS = -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system -lsfml-widgets

$(BUILD_DIR)/gameoflife: $(OBJ_DIR)/main.o $(OBJ_DIR)/game.o $(OBJ_DIR)/board.o $(OBJ_DIR)/case_object.o 
	g++ $(addprefix $(OBJ_DIR)/,$(OBJ)) -L./lib  -o build/gameoflife $(LDLIBS)


$(OBJ_DIR)/main.o : $(SRC_DIR)/main.cpp
	$(CC) $(FLAGS) -c $(SRC_DIR)/main.cpp -o $(OBJ_DIR)/main.o

$(OBJ_DIR)/game.o : $(SRC_DIR)/game.cpp	
	$(CC) $(FLAGS) -c $(SRC_DIR)/game.cpp -o $(OBJ_DIR)/game.o

$(OBJ_DIR)/board.o : $(SRC_DIR)/board.cpp
	$(CC) $(FLAGS) -c $(SRC_DIR)/board.cpp -o $(OBJ_DIR)/board.o

$(OBJ_DIR)/case_object.o : $(SRC_DIR)/case_object.cpp
	$(CC) $(FLAGS) -c $(SRC_DIR)/case_object.cpp -o $(OBJ_DIR)/case_object.o
	
clean: 
	rm $(OBJ_DIR)/*.o
